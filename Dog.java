public class Dog {
	public String breed;
	public int lifeSpan;
	public int averageSpeed;
	
		public void itCanRun() {
			
			if (this.breed.equals("Labrador")) {
				System.out.println("It can run at " + this.averageSpeed + " mph.");
				System.out.println("Wow that is fast!!");
			}else if (this.breed.equals("Chihuahua")) {
				System.out.println("It can run at " + this.averageSpeed + " mph.");
				System.out.println("Its a bit slower...");
			} else {
				System.out.println("Not sure what you're referring to!");
		}
	}
	
			public void itWillLiveFor() {
			
			if (this.breed.equals("Labrador")) {
				System.out.println("A " + this.breed + " will live for an average of " + this.lifeSpan + " years");
			}else if (this.breed.equals("Chihuahua")) {
				System.out.println("A " + this.breed + " will live for an average of " + this.lifeSpan + " years");
				System.out.println("It will live longer");
			} else {
				System.out.println("Not sure what you're referring to!");
		}
	}
}
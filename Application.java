public class Application {
	public static void main(String[] args) {
		
		Dog labrador  = new Dog();
		labrador.breed = "Labrador";
		labrador.lifeSpan = 12;
		labrador.averageSpeed = 28;
		System.out.println("The dog's breed is " + labrador.breed);
		labrador.itWillLiveFor();
		labrador.itCanRun();
		
		Dog chihuahua  = new Dog();
		chihuahua.breed = "Chihuahua";
		chihuahua.lifeSpan = 20;
		chihuahua.averageSpeed = 15;
		System.out.println("The dog's breed is " + chihuahua.breed);
		chihuahua.itWillLiveFor();
		chihuahua.itCanRun();
		
		Dog[] packOfDogs = new Dog[3];
		packOfDogs[0] = new Dog();
		packOfDogs[0].breed = "Morkie";
		packOfDogs[0].lifeSpan = 10;
		packOfDogs[0].averageSpeed = 10;
		
		packOfDogs[1] = new Dog();
		packOfDogs[1].breed = "American Pit Bull Terrier";
		packOfDogs[1].lifeSpan = 8;
		packOfDogs[1].averageSpeed = 30;
		
		packOfDogs[2] = new Dog();
		packOfDogs[2].breed = "Poodle";
		packOfDogs[2].lifeSpan = 15;
		packOfDogs[2].averageSpeed = 25;

		System.out.println(packOfDogs[2].breed);
		System.out.println(packOfDogs[2].lifeSpan);
		System.out.println(packOfDogs[2].averageSpeed);
	}
}